package az.ingress.common.security.errors;

import az.ingress.common.exception.ErrorResponse;
import org.springframework.http.HttpStatus;

public enum Errors implements ErrorResponse {

    JWT_EXPIRED("JWT_EXPIRED", HttpStatus.UNAUTHORIZED, "Jwt expired");

    String key;
    HttpStatus httpStatus;
    String message;

    Errors(String key, HttpStatus httpStatus, String message) {
        this.message = message;
        this.key = key;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
