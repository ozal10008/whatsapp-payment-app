package az.ingress.common.security.auth.services;

import az.ingress.common.exception.ApplicationException;
import az.ingress.common.security.errors.Errors;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static az.ingress.common.HttpConstants.AUTH_HEADER;
import static az.ingress.common.HttpConstants.BEARER_AUTH_HEADER;

@Slf4j
@Service
@RequiredArgsConstructor
public final class TokenAuthService implements AuthService {

    public static final String AUTHORITIES_CLAIM = "authorities";
    private final JwtService jwtService;

    @Override
    public Optional<Authentication> getAuthentication(HttpServletRequest req) {
        return Optional.ofNullable(req.getHeader(AUTH_HEADER))
                .filter(this::isBearerAuth)
                .flatMap(this::getAuthenticationBearer);
    }

    private boolean isBearerAuth(String header) {
        return header.toLowerCase().startsWith(BEARER_AUTH_HEADER.toLowerCase());
    }

    private Optional<Authentication> getAuthenticationBearer(String header) {
        String token = header.substring(BEARER_AUTH_HEADER.length()).trim();

        Claims claims;
        try {
            claims = jwtService.parseToken(token);
        } catch (ExpiredJwtException exception) {
            log.debug("JWT Expired", exception);
            throw new ApplicationException(Errors.JWT_EXPIRED, exception);
        }

        log.trace("The claims parsed {}", claims);
        return Optional.of(getAuthenticationBearer(claims));
    }

    private Authentication getAuthenticationBearer(Claims claims) {
        List<?> authorities = claims.get(AUTHORITIES_CLAIM, List.class);
        return authorities == null
                ? new UsernamePasswordAuthenticationToken(claims.getSubject(), "", List.of())
                : new UsernamePasswordAuthenticationToken(claims.getSubject(), "", getAuthorityList(authorities));
    }

    private List<GrantedAuthority> getAuthorityList(List<?> authorities) {
        return authorities
                .stream()
                .map(a -> new SimpleGrantedAuthority(a.toString()))
                .collect(Collectors.toList());
    }

}
