package az.ingress.common.security;

public enum UserRole {
    ROLE_ANONYMOUS,
    ROLE_USER, //user that has just registered in the web site, not using any services
    ROLE_STUDENT, //user that is enrolled as student in one of the courses
    ROLE_TEACHER, //teacher
    ROLE_MANAGER, //course manager who adds, removes, updates course content
    ROLE_ADMIN, //manages all info in the course
    ROLE_SUPER_USER;

}
