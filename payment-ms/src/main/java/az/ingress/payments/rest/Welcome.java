package main.java.az.ingress.payments.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/welcome")
public class Welcome {

    @GetMapping
    public String sayWelcome() {
        return "Welcome to payments ms 1.0 version";
    }
}
