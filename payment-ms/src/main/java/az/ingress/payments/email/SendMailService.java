package main.java.az.ingress.payments.email;


public interface SendMailService {
    void send(String sendTo, String subject, String htmlBody);
}
