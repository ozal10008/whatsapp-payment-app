package main.java.az.ingress.payments.email;

import lombok.SneakyThrows;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.concurrent.atomic.AtomicInteger;

public class EmailProcessor implements Runnable {

    private ConsumerRecord<String, String> record;
    private AtomicInteger counter;
    private KafkaConsumer<String, String> consumer;

    public EmailProcessor(KafkaConsumer<String, String> consumer,
                          ConsumerRecord<String, String> record,
                          AtomicInteger counter) {
        this.record = record;
        this.counter = counter;
    }

    @Override
    @SneakyThrows
    public void run() {
        System.out.println("Receive message: " + record.value() + ", Partition: "
                + record.partition() + ", Offset: " + record.offset() + ", by ThreadID: "
                + Thread.currentThread().getId());
        counter.incrementAndGet();
        // Thread.sleep(1000);
        System.out.println("Processing failed....");
        if (false)
            throw new RuntimeException("Failed");
    }
}
